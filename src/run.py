# Run the DeskSpoti service

import os
from dotenv import load_dotenv
import subprocess
import requests
import time

load_dotenv()

script_dir = os.path.dirname(__file__)
image_path = os.path.join(script_dir, '../images')

desired_bg_width = int(os.getenv("DESKSPOTI.IMAGE.WIDTH"))
desired_bg_height = int(os.getenv("DESKSPOTI.IMAGE.HEIGHT"))
desired_bg_duration = int(os.getenv("DESKSPOTI.DURATION"))


def command_output(command: str):
    return subprocess.getoutput(command)


def img_requests(width: int, height: int) -> str | bool:
    try:
        response = requests.get("https://picsum.photos/" + str(width) + "/" + str(height))
        file = open(image_path + '/image.jpg', 'wb')
        file.write(response.content)
        file.close()

        return file.name
    except requests.exceptions.RequestException as e:
        return False


def get_colour_scheme() -> str:
    # Get color scheme. There is prefer-light and prefer-dark. We need to know which one to target

    if "dark" in command_output('gsettings get org.gnome.desktop.interface color-scheme'):
        return "picture-uri-dark"
    else:
        return "picture-uri"


def run():
    while True:

        # Get colour mode
        ubuntu_colour_scheme = get_colour_scheme()

        # Get a new random image

        downloaded_bg_image = img_requests(desired_bg_width, desired_bg_height)
        if downloaded_bg_image:
            downloaded_bg_image_uri = "file://" + os.path.abspath(downloaded_bg_image)

            # Set the background to something else

            command_output("gsettings set org.gnome.desktop.background " + ubuntu_colour_scheme + " "
                           + downloaded_bg_image_uri)

        # pause for x seconds

        time.sleep(desired_bg_duration)


# run the program
run()
