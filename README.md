### THIS IS A WIP PROJECT
At present Spotify is not being utilised. The images come from the picsum.com API and not Spotify.

# DeskSpoti

The name _DeskSpoti_ stems from the words "Desktop Background Spotify". It's subject to change when 
should I find a better one!

## What will it be?

It will be a simple service to change my ubuntu desktop background based on my Spotify activity.

## Install

Clone the repo to your local system.

Create the desk-spoti.service systemd service file 

    cp desk-spoti.service.example desk-spoti.service

Change the target path on line 5 to match your installation path.

Move the file to the systemd/system directory.
    
    sudo mv desk-spoti.service /etc/systemd/system/desk-spoti.service

To enable the service run command:

    sudo systemctl enable desk-spoti

This will the start the service next time you restart your machine.

To start the service straight away use:

    sudo systemctl start desk-spoti

To stop the service use:

    sudo systemctl stop desk-spoti

# Future plans

- Learn Python ;)
- Use the Spotify API
- Create dynamic background based on listening preferences

## Author

[Matthew Fedak](https://github.com/matthewfedak)